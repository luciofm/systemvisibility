package mobi.largemind.systemvisibility;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnSystemUiVisibilityChangeListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends Activity {

	View mainView;
	Button btn1;
	Button btn2;

	boolean low_profile = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		mainView = findViewById(R.id.mainView);
		btn1 = (Button) findViewById(R.id.button1);
		btn2 = (Button) findViewById(R.id.button2);

		mainView.setOnSystemUiVisibilityChangeListener(visibilityChanged);

		btn1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				mainView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
				MainActivity.this.getWindow().setFlags(
						WindowManager.LayoutParams.FLAG_FULLSCREEN,
						WindowManager.LayoutParams.FLAG_FULLSCREEN);
			}
		});

		btn2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (!low_profile) {
					MainActivity.this.getWindow().setFlags(
							WindowManager.LayoutParams.FLAG_FULLSCREEN,
							WindowManager.LayoutParams.FLAG_FULLSCREEN);
					mainView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE);
				} else {
					MainActivity.this.getWindow().setFlags(0,
							WindowManager.LayoutParams.FLAG_FULLSCREEN);
					mainView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_VISIBLE);
				}
				low_profile = !low_profile;
			}
		});
	}

	OnSystemUiVisibilityChangeListener visibilityChanged = new OnSystemUiVisibilityChangeListener() {

		@Override
		public void onSystemUiVisibilityChange(int visibility) {
			String str = "";
			if ((visibility & View.SYSTEM_UI_FLAG_HIDE_NAVIGATION) == View.SYSTEM_UI_FLAG_HIDE_NAVIGATION)
				str = "Fullscreen";
			if ((visibility & View.SYSTEM_UI_FLAG_HIDE_NAVIGATION) == 0) {
				str = "Not Fullscreen";
				if (!low_profile)
					MainActivity.this.getWindow().setFlags(0,
							WindowManager.LayoutParams.FLAG_FULLSCREEN);
			}
			if ((visibility & View.SYSTEM_UI_FLAG_LOW_PROFILE) == View.SYSTEM_UI_FLAG_LOW_PROFILE)
				str += " Low Profile";
			Toast.makeText(MainActivity.this, str, Toast.LENGTH_SHORT).show();
		}
	};
}
